##### Lazy Instantiation:
Membuat object hanya ketika developer/programmer membutuhkannya atau hanya membuat
objek ketika runtime
 - Pros: Menghindari program membuat objek yang tidak diperlukan
 - Cons: Dalam multithreading, terdapat kemungkinan program membuat 2 objek padahal
         hanya 1 objek yang diperbolehkan untuk dibuat.
         
##### Eager Instantiation:
Membuat objek ketika aplikasi/sistem pertama kali dijalankan, lalu mengembalikan
objek yang telah dibuat tersebut ketika constructor dipanggil.
 - Pros: Dapat menghindari terbentuknya dua objek pada multithreading
 - Cons: Bisa jadi objek yang dibuat tidak diperlukan, membuang-buang memori dan
         bahkan bisa menyebabkan memory leak pada pembuatan socket atau database 
         connection