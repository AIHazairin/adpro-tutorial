package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InuzumaRamenIngredientTest {
    private Class<?> inuzumaRamenIngredientClass;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenIngredientClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.InuzumaRamenIngredient");
    }

    @Test
    public void testInuzumaRamenIngredientIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(inuzumaRamenIngredientClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIngredientIsAnIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(inuzumaRamenIngredientClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.IngredientFactory")));
    }

    @Test
    public void testOverrideSelectFlavor() throws Exception {
        Method selectFlavor = inuzumaRamenIngredientClass.getDeclaredMethod("selectFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                selectFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                selectFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(selectFlavor.getModifiers()));
    }

    @Test
    public void testOverrideSelectMeat() throws Exception {
        Method selectMeat = inuzumaRamenIngredientClass.getDeclaredMethod("selectMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                selectMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                selectMeat.getParameterCount());
        assertTrue(Modifier.isPublic(selectMeat.getModifiers()));
    }

    @Test
    public void testOverrideSelectNoodle() throws Exception {
        Method selectNoodle = inuzumaRamenIngredientClass.getDeclaredMethod("selectNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                selectNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                selectNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(selectNoodle.getModifiers()));
    }

    @Test
    public void testOverrideSelectTopping() throws Exception {
        Method selectTopping = inuzumaRamenIngredientClass.getDeclaredMethod("selectTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                selectTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                selectTopping.getParameterCount());
        assertTrue(Modifier.isPublic(selectTopping.getModifiers()));
    }

    @Test
    public void testOutput() throws Exception {
        InuzumaRamenIngredient you = new InuzumaRamenIngredient();
        assertTrue(you.selectFlavor().getClass() == Spicy.class);
        assertTrue(you.selectMeat().getClass() == Pork.class);
        assertTrue(you.selectNoodle().getClass() == Ramen.class);
        assertTrue(you.selectTopping().getClass() == BoiledEgg.class);
    }
}
