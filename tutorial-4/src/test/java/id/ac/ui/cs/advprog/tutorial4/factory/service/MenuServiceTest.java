package id.ac.ui.cs.advprog.tutorial4.factory.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class MenuServiceTest {
    private MenuServiceImpl menuService;

    @BeforeEach
    public void setUp() throws Exception {
        menuService = new MenuServiceImpl();
    }

    @Test
    public void testMenuServiceImplHasGetMenusImplementation() {
        assertEquals(4, menuService.getMenus().size());
    }

    @Test
    public void testMenuServiceImplHasCreateMenuImplementation() {
        menuService.createMenu("soba-sobaan", "Soba");
        menuService.createMenu("udon-udonan", "Udon");
        menuService.createMenu("ramen-ramenan", "Ramen");
        menuService.createMenu("shirataki-shiratakian", "Shirataki");

        assertEquals(8, menuService.getMenus().size());
    }

}
