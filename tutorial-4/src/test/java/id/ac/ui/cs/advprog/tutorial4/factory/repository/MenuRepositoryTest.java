package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MenuRepositoryTest {
    private MenuRepository menuRepository;

    @BeforeEach
    public void setUp() {
        menuRepository = new MenuRepository();
    }

    @Test
    public void testAddMenuShouldAddToTheRepository() {
        Menu menu = new SnevnezhaShirataki("dummy");

        menuRepository.add(menu);
        List<Menu> list = menuRepository.getMenus();

        assertThat(list).contains(menu);
        assertThat(list).hasSize(1);
    }

}
