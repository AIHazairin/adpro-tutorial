package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class PorkTest {
    private Class<?> porkClass;

    @BeforeEach
    public void setUp() throws Exception {
        porkClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork");
    }

    @Test
    public void testPorkIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(porkClass.getModifiers()));
    }

    @Test
    public void testPorkIsAMeat() {
        Collection<Type> interfaces = Arrays.asList(porkClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat")));
    }

    @Test
    public void testPorkOverrideGetDescriptionMethod() throws Exception {
        Method getName = porkClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testOutput() throws Exception {
        Pork you = new Pork();
        assertEquals(you.getDescription(), "Adding Tian Xu Pork Meat...");
    }
}
