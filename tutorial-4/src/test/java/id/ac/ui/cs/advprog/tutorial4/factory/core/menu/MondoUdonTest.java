package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonTest {
    private Class<?> mondoUdonClass;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
    }

    @Test
    public void testMondoUdonIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(mondoUdonClass.getModifiers()));
    }

    @Test
    public void testMondoUdonIsAMenu() {
        Collection<Type> superclass = Arrays.asList(mondoUdonClass.getSuperclass());

        assertTrue(superclass.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu")));
    }

    @Test
    public void testOutput() {
        Menu dummy = new MondoUdon("dummy");
        assertEquals("dummy", dummy.getName());
        assertThat(dummy.getNoodle()).isInstanceOf(Udon.class);
        assertThat(dummy.getMeat()).isInstanceOf(Chicken.class);
        assertThat(dummy.getFlavor()).isInstanceOf(Salty.class);
        assertThat(dummy.getTopping()).isInstanceOf(Cheese.class);
    }
}
