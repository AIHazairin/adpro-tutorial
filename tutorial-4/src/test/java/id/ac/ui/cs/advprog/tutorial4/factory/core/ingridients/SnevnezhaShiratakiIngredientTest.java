package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class SnevnezhaShiratakiIngredientTest {
    private Class<?> snevnezhaShiratakiIngredientClass;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiIngredientClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.SnevnezhaShiratakiIngredient");
    }

    @Test
    public void testSnevnezhaShiratakiIngredientIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(snevnezhaShiratakiIngredientClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientIsAnIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(snevnezhaShiratakiIngredientClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.IngredientFactory")));
    }

    @Test
    public void testOverrideSelectFlavor() throws Exception {
        Method selectFlavor = snevnezhaShiratakiIngredientClass.getDeclaredMethod("selectFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                selectFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                selectFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(selectFlavor.getModifiers()));
    }

    @Test
    public void testOverrideSelectMeat() throws Exception {
        Method selectMeat = snevnezhaShiratakiIngredientClass.getDeclaredMethod("selectMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                selectMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                selectMeat.getParameterCount());
        assertTrue(Modifier.isPublic(selectMeat.getModifiers()));
    }

    @Test
    public void testOverrideSelectNoodle() throws Exception {
        Method selectNoodle = snevnezhaShiratakiIngredientClass.getDeclaredMethod("selectNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                selectNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                selectNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(selectNoodle.getModifiers()));
    }

    @Test
    public void testOverrideSelectTopping() throws Exception {
        Method selectTopping = snevnezhaShiratakiIngredientClass.getDeclaredMethod("selectTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                selectTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                selectTopping.getParameterCount());
        assertTrue(Modifier.isPublic(selectTopping.getModifiers()));
    }

    @Test
    public void testOutput() throws Exception {
        SnevnezhaShiratakiIngredient you = new SnevnezhaShiratakiIngredient();
        assertTrue(you.selectFlavor().getClass() == Umami.class);
        assertTrue(you.selectMeat().getClass() == Fish.class);
        assertTrue(you.selectNoodle().getClass() == Shirataki.class);
        assertTrue(you.selectTopping().getClass() == Flower.class);
    }
}
