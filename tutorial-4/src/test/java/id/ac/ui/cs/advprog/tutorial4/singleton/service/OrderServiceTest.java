package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class OrderServiceTest {
    private Class<?> orderServiceClass;

    @Mock
    OrderDrink orderDrink;

    @Mock
    OrderFood orderFood;

    @Spy
    OrderServiceImpl orderService = new OrderServiceImpl();

    @BeforeEach
    public void setUp() throws Exception {
        orderServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderService");
        orderDrink = mock(OrderDrink.class);
        orderFood = mock(OrderFood.class);
    }

    @Test
    public void testOrderADrinkMethodImplementedCorrectly() throws Exception {
        String drink = "dummy";
        doNothing().when(orderDrink).setDrink(isA(String.class));
        when(orderDrink.getDrink()).thenReturn(drink);

        orderService.orderADrink("dummy");
        assertEquals("dummy", orderService.getDrink().getDrink());
    }

    @Test
    public void testOrderAFoodMethodImplementedCorrectly() throws Exception {
        String food = "dummy";
        doNothing().when(orderFood).setFood(isA(String.class));
        when(orderFood.getFood()).thenReturn(food);

        orderService.orderAFood("dummy");
        assertEquals("dummy", orderService.getFood().getFood());
    }
}
