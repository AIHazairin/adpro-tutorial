package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class MenuTest {
    private Class<?> menuClass;

    @BeforeEach
    public void setUp() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void testMenuIsConcreteClass() {
        assertFalse(Modifier.isAbstract(menuClass.getModifiers()));
    }

    @Test
    public void testMenuHasGetNameMethod() throws Exception {
        Method getName = menuClass.getDeclaredMethod("getName");
        assertTrue(Modifier.isPublic(getName.getModifiers()));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void testMenuHasGetNoodleMethod() throws Exception {
        Method getNoodle = menuClass.getDeclaredMethod("getNoodle");
        assertTrue(Modifier.isPublic(getNoodle.getModifiers()));
        assertEquals(0, getNoodle.getParameterCount());
    }

    @Test
    public void testMenuHasgetMeatMethod() throws Exception {
        Method getMeat = menuClass.getDeclaredMethod("getMeat");
        assertTrue(Modifier.isPublic(getMeat.getModifiers()));
        assertEquals(0, getMeat.getParameterCount());
    }

    @Test
    public void testMenuHasgetToppingMethod() throws Exception {
        Method getTopping = menuClass.getDeclaredMethod("getTopping");
        assertTrue(Modifier.isPublic(getTopping.getModifiers()));
        assertEquals(0, getTopping.getParameterCount());
    }

    @Test
    public void testMenuHasgetFlavorMethod() throws Exception {
        Method getFlavor = menuClass.getDeclaredMethod("getFlavor");
        assertTrue(Modifier.isPublic(getFlavor.getModifiers()));
        assertEquals(0, getFlavor.getParameterCount());
    }

    @Test
    public void testOutputInuzuma() throws Exception {
        Menu dummy = new InuzumaRamen("dummy menu");
        assertEquals("dummy menu", dummy.getName());
        assertEquals("Adding Guahuan Boiled Egg Topping", dummy.getTopping().getDescription());
        assertEquals("Adding Inuzuma Ramen Noodles...", dummy.getNoodle().getDescription());
        assertEquals("Adding Tian Xu Pork Meat...", dummy.getMeat().getDescription());
        assertEquals("Adding Liyuan Chili Powder...", dummy.getFlavor().getDescription());
    }
}
