package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class IngredientFactoryTest {
    private Class<?> ingredientFactoryClass;

    @BeforeEach
    public void setup() throws Exception {
        ingredientFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.IngredientFactory");
    }

    @Test
    public void testIngredientFactoryIsAPublicInterface() {
        int classModifiers = ingredientFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testIngredientFactoryHasSelectFlavor() throws Exception {
        Method getName = ingredientFactoryClass.getDeclaredMethod("selectFlavor");
        int methodModifiers = getName.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void testIngredientFactoryHasSelectMeat() throws Exception {
        Method getName = ingredientFactoryClass.getDeclaredMethod("selectMeat");
        int methodModifiers = getName.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void testIngredientFactoryHasSelectTopping() throws Exception {
        Method getName = ingredientFactoryClass.getDeclaredMethod("selectTopping");
        int methodModifiers = getName.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void testIngredientFactoryHasSelectNoodle() throws Exception {
        Method getName = ingredientFactoryClass.getDeclaredMethod("selectNoodle");
        int methodModifiers = getName.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getName.getParameterCount());
    }
}
