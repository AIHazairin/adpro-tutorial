package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderFoodTest {
    private OrderFood orderFood;

    @Test
    public void testOrderDrinkOnlyCreatedOnce() {
        OrderFood dummy1 = orderFood.getInstance();
        OrderFood dummy2 = OrderFood.getInstance();

        assertThat(dummy1).isEqualToComparingFieldByField(dummy2);
    }
}

