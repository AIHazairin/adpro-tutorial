package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LiyuanSobaIngredientTest {
    private Class<?> liyuanSobaIngredientClass;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaIngredientClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.LiyuanSobaIngredient");
    }

    @Test
    public void testLiyuanSobaIngredientIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(liyuanSobaIngredientClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIngredientIsAnIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(liyuanSobaIngredientClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.IngredientFactory")));
    }

    @Test
    public void testOverrideSelectFlavor() throws Exception {
        Method selectFlavor = liyuanSobaIngredientClass.getDeclaredMethod("selectFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                selectFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                selectFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(selectFlavor.getModifiers()));
    }

    @Test
    public void testOverrideSelectMeat() throws Exception {
        Method selectMeat = liyuanSobaIngredientClass.getDeclaredMethod("selectMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                selectMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                selectMeat.getParameterCount());
        assertTrue(Modifier.isPublic(selectMeat.getModifiers()));
    }

    @Test
    public void testOverrideSelectNoodle() throws Exception {
        Method selectNoodle = liyuanSobaIngredientClass.getDeclaredMethod("selectNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                selectNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                selectNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(selectNoodle.getModifiers()));
    }

    @Test
    public void testOverrideSelectTopping() throws Exception {
        Method selectTopping = liyuanSobaIngredientClass.getDeclaredMethod("selectTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                selectTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                selectTopping.getParameterCount());
        assertTrue(Modifier.isPublic(selectTopping.getModifiers()));
    }

    @Test
    public void testOutput() throws Exception {
        LiyuanSobaIngredient you = new LiyuanSobaIngredient();
        assertTrue(you.selectFlavor().getClass() == Sweet.class);
        assertTrue(you.selectMeat().getClass() == Beef.class);
        assertTrue(you.selectNoodle().getClass() == Soba.class);
        assertTrue(you.selectTopping().getClass() == Mushroom.class);
    }
}
