package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FishTest {
    private Class<?> fishClass;

    @BeforeEach
    public void setUp() throws Exception {
        fishClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish");
    }

    @Test
    public void testFishIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(fishClass.getModifiers()));
    }

    @Test
    public void testFishIsAMeat() {
        Collection<Type> interfaces = Arrays.asList(fishClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat")));
    }

    @Test
    public void testFishOverrideGetDescriptionMethod() throws Exception {
        Method getName = fishClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testOutput() throws Exception {
        Fish you = new Fish();
        assertEquals(you.getDescription(), "Adding Zhangyun Salmon Fish Meat...");
    }
}
