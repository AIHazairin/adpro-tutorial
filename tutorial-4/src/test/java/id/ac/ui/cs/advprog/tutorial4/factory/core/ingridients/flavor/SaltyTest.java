package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class SaltyTest {
    private Class<?> saltyClass;

    @BeforeEach
    public void setUp() throws Exception {
        saltyClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty");
    }

    @Test
    public void testSaltyIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(saltyClass.getModifiers()));
    }

    @Test
    public void testSaltyIsAFlavor() {
        Collection<Type> interfaces = Arrays.asList(saltyClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor")));
    }

    @Test
    public void testSaltyOverrideGetDescriptionMethod() throws Exception {
        Method getName = saltyClass.getDeclaredMethod("getDescription");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testOutput() throws Exception {
        Salty you = new Salty();
        assertEquals(you.getDescription(), "Adding a pinch of salt...");
    }

}
