package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class MondoUdonIngredientTest {
    private Class<?> mondoUdonIngredientClass;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonIngredientClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.MondoUdonIngredient");
    }

    @Test
    public void testMondoUdonIngredientIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(mondoUdonIngredientClass.getModifiers()));
    }

    @Test
    public void testMondoUdonIngredientIsAnIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(mondoUdonIngredientClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.IngredientFactory")));
    }

    @Test
    public void testOverrideSelectFlavor() throws Exception {
        Method selectFlavor = mondoUdonIngredientClass.getDeclaredMethod("selectFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                selectFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                selectFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(selectFlavor.getModifiers()));
    }

    @Test
    public void testOverrideSelectMeat() throws Exception {
        Method selectMeat = mondoUdonIngredientClass.getDeclaredMethod("selectMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                selectMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                selectMeat.getParameterCount());
        assertTrue(Modifier.isPublic(selectMeat.getModifiers()));
    }

    @Test
    public void testOverrideSelectNoodle() throws Exception {
        Method selectNoodle = mondoUdonIngredientClass.getDeclaredMethod("selectNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                selectNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                selectNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(selectNoodle.getModifiers()));
    }

    @Test
    public void testOverrideSelectTopping() throws Exception {
        Method selectTopping = mondoUdonIngredientClass.getDeclaredMethod("selectTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                selectTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                selectTopping.getParameterCount());
        assertTrue(Modifier.isPublic(selectTopping.getModifiers()));
    }

    @Test
    public void testOutput() throws Exception {
        MondoUdonIngredient you = new MondoUdonIngredient();
        assertTrue(you.selectFlavor().getClass() == Salty.class);
        assertTrue(you.selectMeat().getClass() == Chicken.class);
        assertTrue(you.selectNoodle().getClass() == Udon.class);
        assertTrue(you.selectTopping().getClass() == Cheese.class);
    }
}
