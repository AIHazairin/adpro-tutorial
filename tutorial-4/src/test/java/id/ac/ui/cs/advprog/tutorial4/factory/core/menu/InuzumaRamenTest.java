package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenTest {
    private Class<?> inuzumaRamenClass;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(inuzumaRamenClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIsAMenu() {
        Collection<Type> superclass = Arrays.asList(inuzumaRamenClass.getSuperclass());

        assertTrue(superclass.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu")));
    }

    @Test
    public void testOutput() {
        Menu dummy = new InuzumaRamen("dummy");
        assertEquals("dummy", dummy.getName());
        assertThat(dummy.getNoodle()).isInstanceOf(Ramen.class);
        assertThat(dummy.getMeat()).isInstanceOf(Pork.class);
        assertThat(dummy.getFlavor()).isInstanceOf(Spicy.class);
        assertThat(dummy.getTopping()).isInstanceOf(BoiledEgg.class);
    }
}
