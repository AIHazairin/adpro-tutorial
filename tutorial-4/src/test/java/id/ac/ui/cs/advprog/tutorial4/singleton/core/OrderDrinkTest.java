package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderDrinkTest {
    private OrderDrink orderDrink;

    @Test
    public void testOrderDrinkOnlyCreatedOnce() {
        OrderDrink dummy1 = OrderDrink.getInstance();
        OrderDrink dummy2 = OrderDrink.getInstance();

        assertThat(dummy1).isEqualToComparingFieldByField(dummy2);
    }

}
