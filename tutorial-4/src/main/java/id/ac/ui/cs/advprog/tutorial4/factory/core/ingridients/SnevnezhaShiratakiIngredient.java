package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public class SnevnezhaShiratakiIngredient implements IngredientFactory {

    public Flavor selectFlavor() {
        return new Umami();
    }

    public Meat selectMeat() {
        return new Fish();
    }

    public Noodle selectNoodle() {
        return new Shirataki();
    }

    public Topping selectTopping() {
        return new Flower();
    }
}
