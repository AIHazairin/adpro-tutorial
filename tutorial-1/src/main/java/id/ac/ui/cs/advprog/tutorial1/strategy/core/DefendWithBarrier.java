package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    //ToDo: Complete me
    public DefendWithBarrier() {}

    @Override
    public String defend() {
        return "you cant see me, you cant hit me";
    }

    @Override
    public String getType() {
        return "DefendWithBarrier";
    }
}
