package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    //ToDo: Complete me
    public DefendWithShield() {}

    @Override
    public String defend() {
        return "not today, buddy!";
    }

    @Override
    public String getType() {
        return "DefendWithShield";
    }
}
