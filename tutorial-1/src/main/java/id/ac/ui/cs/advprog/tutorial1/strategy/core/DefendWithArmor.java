package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    //ToDo: Complete me
    public DefendWithArmor() {}

    @Override
    public String defend() {
        return "you cant scratch me even an inch, bruh";
    }

    @Override
    public String getType() {
        return "DefendWithArmor";
    }
}
