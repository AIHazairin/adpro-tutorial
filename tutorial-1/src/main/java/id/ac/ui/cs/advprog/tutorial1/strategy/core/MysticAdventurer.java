package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {

    //ToDo: Complete me
    private String alias;

    public MysticAdventurer(){
        super();
        this.alias = "Mystic";
        setAttackBehavior(new AttackWithMagic());
        setDefenseBehavior(new DefendWithShield());
    }

    @Override
    public String getAlias() {
        return this.alias;
    }
}
