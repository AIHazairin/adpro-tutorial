package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean aimMode;

    public BowAdapter(Bow bow) {
        super();
        this.bow = bow;
        aimMode = false;
    }

    @Override
    public String normalAttack() {
        return this.bow.getHolderName() + " attacked with " + this.bow.getName()
                + " (normal attack): " + bow.shootArrow(aimMode);
    }

    @Override
    public String chargedAttack() {
        aimMode = !aimMode;
        if (aimMode) {
            return this.bow.getHolderName() + " attacked with " + this.bow.getName()
                    + " (charged attack): Entered aim shot mode";
        } else {
            return this.bow.getHolderName() + " attacked with " + this.bow.getName()
                    + " (charged attack): Leaving aim shot mode";
        }
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return bow.getHolderName();
    }
}
