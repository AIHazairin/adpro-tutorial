package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class ApakeTransformation {

    public ApakeTransformation() {}

    public Spell encode(Spell spell) { return process(spell, true); }

    public Spell decode(Spell spell) { return process(spell, false); }

    private Spell process(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int n = text.length();
        char[] res = new char[n];
        for(int i = 0; i < n; i = i+2) {
            if(i+1 != n) {
                res[i+1] = text.charAt(i);
                res[i] = text.charAt(i+1);
            } else {
                res[i] = text.charAt(i);
            }
        }
        return new Spell(new String(res), codex);
    }
}
