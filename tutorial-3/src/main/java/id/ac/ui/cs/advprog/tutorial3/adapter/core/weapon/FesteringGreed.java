package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class FesteringGreed implements Weapon {

    private String holderName;

    public FesteringGreed(String holderName) {
        this.holderName = holderName;
    }
    @Override
    public String normalAttack() {
        // TODO: complete me
        return this.holderName + " attacked with " + getName() +
                " (normal attack): MALU KAMU SAMA ANAK BU DIAN!";
    }

    @Override
    public String chargedAttack() {
        // TODO: complete me
        return this.holderName + " attacked with " + getName() +
                " (charged attack): IBU CORET KAMU DARI KK!";
    }

    @Override
    public String getName() {
        return "Festering Greed";
    }

    @Override
    public String getHolderName() { return holderName; }
}
