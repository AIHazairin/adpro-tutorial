package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ApakeTransformationTest {
    public Class<?> apakeClass;

    @BeforeEach
    public void setup() throws Exception {
        apakeClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.ApakeTransformation");
    }

    @Test
    public void testApakeHasEncodeMethod() throws Exception {
        Method translate = apakeClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testApakeEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "aSifara dnI w ne tota b alkcmsti hotf roego rus owdr";

        Spell result = new ApakeTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

//    @Test
//    public void testApakeEncodesCorrectlyWithCustomKey() throws Exception {
//        String text = "Safira and I went to a blacksmith to forge our sword";
//        Codex codex = AlphaCodex.getInstance();
//        Spell spell = new Spell(text, codex);
//        String expected = "Eakq8aimAu 5 1m4ti5BQawbqitkQy6 hwttHwoPs2QogrE0CoPp";
//
//        Spell result = new ApakeTransformation("SafiraEmyra").encode(spell);
//        assertEquals(expected, result.getText());
//    }

    @Test
    public void testApakeHasDecodeMethod() throws Exception {
        Method translate = apakeClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testApakeDecodesCorrectly() throws Exception {
        String text = "aSifara dnI w ne tota b alkcmsti hotf roego rus owdr";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new ApakeTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

//    @Test
//    public void testApakeDecodesCorrectlyWithCustomKey() throws Exception {
//        String text = "Eakq8aimAu 5 1m4ti5BQawbqitkQy6 hwttHwoPs2QogrE0CoPp";
//        Codex codex = AlphaCodex.getInstance();
//        Spell spell = new Spell(text, codex);
//        String expected = "Safira and I went to a blacksmith to forge our sword";
//
//        Spell result = new ApakeTransformation("SafiraEmyra").decode(spell);
//        assertEquals(expected, result.getText());
//    }
}
